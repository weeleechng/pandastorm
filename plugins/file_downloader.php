<?php

# get correct id for plugin
$thisfile = basename(__FILE__, '.php');

# register plugin
register_plugin(
  $thisfile,
  'File downloader',
  '0.1',
  'estorm International',
  'http://www.estorm.com',
  'Download a file from the data/uploads folder',
  'pages',
  'download_request_start'
);


# hooks
add_action ('index-post-dataindex', 'download_request_start');

function download_request_start () 
{
  if ($_GET['file'] != '')
  {
    $filename = $_GET['file'];
    $pdf_file = 'data/uploads/' . $filename;
    // $pdf_file = $base_url . $pdf_file;

    if (file_exists ($pdf_file))
    {
      header ('Content-Type: application/pdf');
      header ('Content-disposition: attachment; filename="' . $filename . '"');
      readfile ($pdf_file);
      exit ();
    } 
    else 
    {
      print "<code>Our apologies, there seems to be a problem accessing the file.</code>";
      exit ();
    }
  }
}
