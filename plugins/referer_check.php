<?php

# get correct id for plugin
$thisfile = basename(__FILE__, '.php');

# register plugin
register_plugin(
  $thisfile,
  'Referer check',
  '0.1',
  'estorm International',
  'http://www.estorm.com',
  'Ensure that this page is loaded from the form submission',
  'pages',
  'referer_check_start'
);


# hooks
add_action ('index-post-dataindex', 'referer_check_start');

/* redirect user to form page if this page was accessed directly */
function referer_check_start ($from = 'index', $check = 'panda-complete') 
{
  if (get_page_slug (false) == $check && $_GET['storm_mode'] != 'debug')
  {
    if ($_SERVER['HTTP_REFERER'] != find_url ($from))
    {
      redirect (find_url ($from));
    }
  }
}
