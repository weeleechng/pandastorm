<?php

# get correct id for plugin
$thisfile = basename(__FILE__, '.php');

# register plugin
register_plugin(
  $thisfile,
  'Mail to address',
  '1.0.1',
  'estorm International',
  'http://www.estorm.com',
  'Send email to given address in location bar',
  'pages',
  'mail_start'
);


# hooks
add_action('pages-sidebar', 'createSideMenu', array ($thisfile, 'Email to address'));
add_action ('index-posttemplate', 'checkAndSendEmailToAddress');

# definitions
define (CONFIGFILE, GSDATAOTHERPATH . 'mail_toaddress.xml');

function mail_start() 
{
  if (isset ($_GET['save']) && $_GET['save'] == 'config')
  {
    saveEmailConfig ();
  }
  else
  {
    mailOverview();
  }
}

function saveEmailConfig ()
{
  echo writeEmailConfig ();
  mailOverview ();
}

function writeEmailConfig ()
{
  $from = $_POST['mail-from'];
  $subject = $_POST['mail-subject'];
  $body = $_POST['mail-body'];

  $xml = @new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><item></item>');
  $xml->addChild('from', $from);
  $xml->addChild('subject', htmlspecialchars($subject));
  $xml->addChild('body', safe_slash_html($body));    
  XMLsave ($xml, CONFIGFILE);

  if (!is_writable(CONFIGFILE))
  {
    return '<div class="error">Unable to write config to file</div>';
  }
  else
  {
    return '<div class="updated">Config has been succesfully saved</div>';
  }
} 

function loadEmailConfig ()
{
  $data = array();

  if (file_exists (CONFIGFILE)) 
  {
    $xml = @getXML (CONFIGFILE);

    if (!empty ($xml))
    {
      $data['from'] = $xml->from;
      $data['subject'] = $xml->subject;
      $data['body'] = @stripslashes ($xml->body);
    }
    else
    {
      $data['error'] = "Error reading " .CONFIGFILE;
    }
  }
 
  return $data; 
}

function default_values ($data)
{
  if (!isset ($data['from']) || $data['from'] == '')
  {
    $data['from'] = 'info@estorm.com';
  }

  if (!isset ($data['subject']) || $data['subject'] == '')
  {
    $data['subject'] = 'estorm International - Panda 4.0 Recovery Guide';
  }

  if (!isset ($data['body']) || $data['body'] == '')
  {
    $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    $base_pattern = '/(http\:\/\/\S+\/)admin\S+/';
    preg_match ($base_pattern, $url, $matches);
    $site_url = $matches[1];
    $download_url =  $site_url . '?file=recoverfromgoogleupdatepanda4.pdf';
    $data['body'] = 'Hello,&#13;&#13;Thank you for your interest in the Panda 4.0 Recovery Guide.&#13;&#13;You may download the document <a href="' . $download_url . '">here</a>.&#13;&#13;Regards,&#13;estorm International';
  }  

  return $data;
}

/*******************************************************
 * @function mailOverview
 * @action show main screen of mail newsletter*/
function mailOverview() 
{   
	$data = default_values (loadEmailConfig ());

?>
  <form action="load.php?id=mail_to_address&save=config" method="post" accept-charset="utf-8">
    <p>
      <b>From:</b><br />
      <input name="mail-from" size="90" type="text" value="<?php echo $data['from']; ?>">
    </p>

  	<p>
    	<b>Subject:</b><br />
    	<input name="mail-subject" size="90" type="text" value="<?php echo $data['subject']; ?>">
  	</p>

  	<p>
    	<b>Message:</b><br />
    	<textarea rows="25" id="mail-body" name="mail-body"><?php echo $data['body']; ?></textarea>
  	</p>
  	<p>
    	<input name="submit" type="submit" class="submit" value=" Save " />
  	</p>
	</form>
  <script type="text/javascript" src="template/js/codemirror/lib/codemirror-compressed.js"></script>
    <link rel="stylesheet" href="template/js/codemirror/lib/codemirror.css">
    <link rel="stylesheet" href="template/js/codemirror/theme/default.css">

    <script type="text/javascript">
    window.onload = function() 
    {
      var editor = CodeMirror.fromTextArea (document.getElementById ("mail-body"), 
      {
        lineNumbers: false,
        lineWrapping: false,
        matchBrackets: true,
        indentUnit: 2,
        indentWithTabs: false,
        enterMode: "keep",
        mode:"application/x-httpd-php",
        tabMode: "shift"
      });
    }  
    </script>
<?php
}

/* execute sending of email */
function checkAndSendEmailToAddress () 
{
  if (isset ($_GET['sendto']) && $_GET['sendto'] != '' && filter_var ($_GET['sendto'], FILTER_VALIDATE_EMAIL))
  { 
    $data = loadEmailConfig();
    $to = $_GET['sendto'];

    $subject = $data['subject'];
    $random_hash = md5(date('r', time())); 
    $body  = "--PHP-alt-" . $random_hash;
    $body .= "\nContent-Type: text/html; charset=utf-8";
    $body .= "\nContent-Transfer-Encoding: 8bit\n\n";
    $body .= $data['body'];  
    $body .= "\n--PHP-alt-" . $random_hash . "--\n";
    $header  = "From: " . $data['from'];
    $header .= "\r\nContent-Type: multipart/alternative; boundary=\"PHP-alt-".$random_hash."\"";  
   
    if (mail ($to, '=?UTF-8?B?'.base64_encode($subject).'?=', $body, $header))
    {
      echo ("<!-- Email sent! -->");
    }
    else 
    {
      echo ("<!-- Email not sent -->");
    }
  }

} 
