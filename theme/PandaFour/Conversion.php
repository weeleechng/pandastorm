<?php 

if (!defined ('IN_GS'))
{ 
  die ('You cannot load this page directly.'); 
}

/****************************************************
*
* @File: 		template.php
* @Package:	GetSimple
* @Action:	Panda estorm theme for GetSimple CMS
* @Author:  estorm International http://www.estorm.com/
*
*****************************************************/

/* http://www.cyberiada.org/cnb/log/check-if-a-component-exists-in-getsimple/ */
if (!function_exists ('component_exists'))
{
  function component_exists ($id) 
  {
    global $components;
    if (!$components) 
    {
      if (file_exists (GSDATAOTHERPATH.'components.xml')) 
      {
        $data = getXML (GSDATAOTHERPATH.'components.xml');
        $components = $data->item;
      } 
      else 
      {
        $components = array();
      }
    }

    $exists = FALSE;
    
    if (count ($components) > 0) 
    {
      foreach ($components as $component) 
      {
        if ($id == $component->slug) 
        {
          $exists = TRUE;
          break;
        }
      }
    }
    return $exists;
  }
}

# Include the header template
include('header.inc.php'); 
?>	
    <article class="teaser">
      <?php if (component_exists ('conversion-copy')) get_component('conversion-copy'); ?>
    </article>
    </div><!-- / div.left -->
    <div class="right">
      <?php get_page_content(); ?>
    </div>
    <div class="clear"></div>
  </div><!-- / div.container -->
  <div id="storm-ajax-throbber"></div>
<?php 
#include the footer template
include('footer.inc.php'); 
?>
