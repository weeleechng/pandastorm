var article_first_paragraph = $('article.teaser p:nth-of-type(1)');

function readmore_baseheight ()
{
  if ($(window).innerWidth() < 960)
  {
    var start_expanded = $('article.teaser').hasClass ('expanded') ? true : false;

    $('article.teaser').readmore (
    {
      moreLink: '<a href="#">More</a>',
      maxHeight: $(article_first_paragraph).height() + $(article_first_paragraph).position().top,
      startOpen: start_expanded,
      expandedClass: 'expanded',
      collapsedClass: 'collapsed',
      speed: 500,
    });

  }
  else
  {
    $('article.teaser').readmore('destroy');
    $('a.readmore-js-toggle').removeClass ('collapse, expand');
  }
}

$(window).load (function ()
{
  readmore_baseheight (); 
});

$(window).resize (function ()
{
  readmore_baseheight (); 
});
