<?php 

if (!defined ('IN_GS'))
{ 
  die ('You cannot load this page directly.'); 
}

/****************************************************
*
* @File: 		header.inc.php
* @Package:		GetSimple
* @Action:		Arsenic theme for GetSimple CMS
* @Author:      Oleg Svetlov http://getsimplecms.ru/
*
*****************************************************/

?><!DOCTYPE html>
<head>
  <meta charset="utf-8">	
  <title><?php component_exists ('seo-html-title') ? get_component('seo-html-title') : get_page_clean_title (); ?> - <?php get_site_name(); ?></title>	
	<?php get_header(); ?>
  <meta name="description" content="<?php component_exists ('meta-description') ? get_component('meta-description') : '' ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <link rel="shortcut icon" href="http://resources.estorm.com/icon/favicon.png" type="image/png" />
  <link href="<?php get_theme_url(); ?>/css/main.css" rel="stylesheet" type="text/css">
  <link href="<?php get_theme_url(); ?>/css/storm_ajax_throbber.css" rel="stylesheet" type="text/css">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
</head>

<body id="<?php get_page_slug(); ?>">

<div class="container">
  <div class="left">
    
    <header>
      <div class="logo">
        <a id="logo" href="http://www.estorm.com" target="_blank"><img alt="<?php get_site_name(); ?>" src="<?php get_theme_url(); ?>/images/logo.png"></a>
      </div>
      <div class="desc"><?php get_component('tagline'); ?></div>
      <div class="clear"></div>
    </header>

