<?php 

if (!defined ('IN_GS'))
{ 
  die ('You cannot load this page directly.'); 
}

/****************************************************
*
* @File: 		footer.inc.php
* @Package:		GetSimple
* @Action:		Arsenic theme for GetSimple CMS
* @Author:      Oleg Svetlov http://getsimplecms.ru/
*
*****************************************************/
?>
	<footer>
  	<div class="links">
      <a href="http://www.estorm.com/about-us" target="_blank">About estorm</a> | 
      <a href="http://www.estorm.com/digital-marketing-services/seo" target="_blank">SEO Services</a> |<br> 
      <a href="http://www.estorm.com/success-stories/work" target="_blank">Case Studies</a> | 
      <a href="http://www.estorm.com/contact-us" target="_blank">Contact estorm</a>
    </div>
    <div class="social">
      <a href="http://www.facebook.com/estormInternational" target="_blank"><img src="<?php get_theme_url(); ?>/images/icon_facebook.png" width="20" height="20" alt=""/></a>
      <a href="http://twitter.com/estormintl" target="_blank"><img src="<?php get_theme_url(); ?>/images/icon_twitter.png" width="20" height="20" alt=""/></a> 
      <a href="http://www.linkedin.com/company/e-storm-international" target="_blank"><img src="<?php get_theme_url(); ?>/images/icon_linkedin.png" width="20" height="20" alt=""/></a>
    </div>
    <div class="copyright">Copyright © 1998 - 2015 e-storm International Corp. <br>All rights reserved.</div>
    <span class="clear"></span>
  </footer>

  <?php get_footer(); ?>

<!--a id="gotoTop" href="#">&and;</a-->
<script src="<?php get_theme_url(); ?>/js/readmore.edit.min.js"></script>
<script src="<?php get_theme_url(); ?>/js/panda_expander.js"></script>
<script src="<?php get_theme_url(); ?>/js/storm_ajax_throbber.js"></script>

<!-- Google Tag Manager -->
<noscript>
  <iframe src="//www.googletagmanager.com/ns.html?id=GTM-5SRJ9X" height="0" width="0" style="display:none; visibility:hidden;"></iframe>
</noscript>
<script>
(function (w,d,s,l,i) 
{
  w[l] = w[l] || []; 
  w[l].push ({'gtm.start': new Date ().getTime (), event:'gtm.js'});
  var f = d.getElementsByTagName(s)[0], 
  j = d.createElement(s), 
  dl = l != 'dataLayer' ? '&l=' + l : '';
  j.async = true;
  j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;
  f.parentNode.insertBefore (j, f);
})
(window, document, 'script', 'dataLayer', 'GTM-5SRJ9X');
</script>
<!-- End Google Tag Manager -->

</body>
</html>
